﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfMT
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetZones/{from}/{to}")]
        Zones[] GetZones(string from, string to);

        // TODO: Add your service operations here
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }

    [DataContract(Name = "Zones")]
    public class Zones
    {
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string From_Zone { get; set; }
        [DataMember]
        public string To_Zone { get; set; }
        [DataMember]
        public decimal DiscountRate { get; set; }
        [DataMember]
        public decimal MinimunCharge { get; set; }
    }
}
