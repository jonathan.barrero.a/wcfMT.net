﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WcfMT.DataLayer;

namespace WcfMT
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        private MTDBEntities db = new MTDBEntities();

        public Zones[] GetZones(string from, string to)
        {
            var result = db.sp_GetZones(from, to).Select(z => new Zones
            {
                ZoneId = z.ZoneId,
                From_Zone = z.From_Zone,
                To_Zone = z.To_Zone,
                DiscountRate = z.DiscountRate ?? 0,
                MinimunCharge = z.MinimunCharge ?? 0
            });
            

            return result.ToArray();
        }
    }
}
